# Git Pre-Commit Check

## Overview

Will check for:

* python syntax `(.py)`
* initial mixed space/tab character
* unnecessary trailing spaces
* js syntax `(.js)`
* puppet manifest `(.pp)`
* puppet template `(.erb)`


## Installation

Download this script and put somewhere, then run this under any git repo

    ~/src/somegitrepo $ /pathto/precommitcheck.py -i


## Prerequisite

### flake8
Python checker. Combination of PyFlakes, pep8 and Ned Batchelder's McCabe
script.

Certain unnecessary errors are ignored. (hard-coded)

To install:

    sudo pip install -U flake8


### jshint
Js checker. *Prerequisite*: Need nodejs and npm.

To install:

    sudo npm install -g jshint

To install ``nodejs`` and ``npm`` in modern *Ubuntus* derivatives, just do:

    sudo apt-get install nodejs npm nodejs-legacy

To install in old *Debian* or others, just download the source code from nodejs
site and compile - will include both nodejs and npm together once done.


### puppet
Puppet manifest syntax checker (any .pp).

To install in modern *Ubuntus* derivates, just do:

    sudo apt-get install puppet


## Complains
If there's any complains, bugs, or useful stuff to add checks, or even suppress
certain warning, just file a bug or fork/pull-requests.


