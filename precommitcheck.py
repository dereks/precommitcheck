#!/usr/bin/env python

import os
import re
import subprocess
import unittest
from argparse import ArgumentParser


# maximum error to stop further checking
MAX_ERROR = 1000

IGNORE_3RDPARTY_FILES = [
    '\.git/.*$',
    '.*/generated/.*$',
    '.*/haproxy/src/.*',
    '.*/angular-.*\.js$',
    '.*/jquery.*\.js$',
    '.*/ui-bootstrap-.*\.js$',
    '.*/xregexp-.*\.js$',
    '.*/respond\.min\.js$',
    '.*/es5-shim\.js$',
    '.*/sprintf-.*\.js$',
    '.*/sitecatalyst/.*\.js$',
    '.*/jscolor\.js$',
    '.*/tests/out/coverage/.*$',
    '.*/tests/unit/.*\.js$',
    '.*/axurerp_.*\.js$',
    '.*/axure.*\.css$',
    '.*/jquery.*\.css$',
]

STD_SOURCE_CODE = [
    '.*\.c$',
    '.*\.cfg$',
    '.*\.conf$',
    '.*\.cpp$',
    '.*\.css$',
    '.*\.erb$',
    '.*\.h$',
    '.*\.hpp$',
    '.*\.html$',
    '.*\.ini$',
    '.*\.java$',
    '.*\.js$',
    '.*\.md$',
    '.*\.php$',
    '.*\.pp$',
    '.*\.py$',
    '.*\.rst$',
    '.*\.txt$',
]

ANSI_BOLD   = '\x1b[01m'
ANSI_RESET  = '\x1b[00m'
ANSI_RED_BG = '\x1b[41m'
UTF8_DOUBLE_GREATER_THAN = '\xc2\xbb'
UTF8_MIDDLE_DOT = '\xc2\xb7'


def check_trailing_whitespace(filename):
    trailspace = re.compile('[\t ]+$')
    affected_line = []
    err = None
    try:
        with open(filename) as f:
            linecount = 1
            for line in f:
                r = trailspace.search(line)
                if r:
                    e = line[r.start():r.end()]
                    e = e.replace('\t', UTF8_DOUBLE_GREATER_THAN)
                    e = e.replace(' ', UTF8_MIDDLE_DOT)
                    s = line[:r.start()] + ANSI_RED_BG + e + ANSI_RESET + line[r.end():]
                    affected_line.append('%s:%s' % (linecount, s.rstrip('\r\n')))
                linecount += 1
    except OSError as ex:
        err = ex
    return '\n'.join(affected_line), err


def check_starting_tab(filename):
    affected_line = []
    err = None
    try:
        with open(filename) as f:
            linecount = 1
            for line in f:
                flag = 0
                for i, c in enumerate(line):
                    if c == '\t':
                        flag |= 0x01
                    elif c == ' ':
                        flag |= 0x02
                    else:
                        break
                if (flag & 0x01) == 0x01:   # mix tab & space at the front
                    e = line[:i]
                    e = e.replace('\t', UTF8_DOUBLE_GREATER_THAN)
                    e = e.replace(' ', UTF8_MIDDLE_DOT)
                    s = ANSI_RED_BG + e + ANSI_RESET + line[i:]
                    affected_line.append('%s:%s' % (linecount, s.rstrip('\r\n')))
                linecount += 1
    except OSError as ex:
        err = ex
    return '\n'.join(affected_line), err


CHECKS = [
    {
        'output': 'Python static checker flake8...',
        'command': 'flake8 --ignore=E501,E241,E201,E202,E203,E265,E303,E124,E128,E261,E701,E262,W391,E126,E127,E221,E271 %s',
        'match_files': ['.*\.py$'],
        'ignore_files': IGNORE_3RDPARTY_FILES,
        'print_filename': False,
    },
    {
        'output': 'Trailing whitespace...',
        'command': check_trailing_whitespace,
        'match_files': STD_SOURCE_CODE,
        'ignore_files': IGNORE_3RDPARTY_FILES,
        'print_filename': True,
    },
    {
        'output': 'Starting tabs or mixed tab/space...',
        'command': check_starting_tab,
        'match_files': STD_SOURCE_CODE,
        'ignore_files': ['.*/makefile$', '.*/Makefile$'] + IGNORE_3RDPARTY_FILES,
        'print_filename': True,
    },
    {
        'output': 'JS hint...',
        'command': 'jshint %s',
        'match_files': ['.*\.js$'],
        'ignore_files': IGNORE_3RDPARTY_FILES,
        'print_filename': False,
    },
    {
        'output': 'Puppet parser validate...',
        'command': 'puppet parser validate %s',
        'match_files': ['.*\.pp$'],
        'ignore_files': IGNORE_3RDPARTY_FILES,
        'print_filename': True,
    },
    {
        'output': 'Puppet ERB template...',
        'command': 'erb -x -T - \'%s\' | ruby -c',
        'match_files': ['.*\.erb$'],
        'ignore_files': IGNORE_3RDPARTY_FILES,
        'print_filename': True,
    },
]


GIT_STATUS_SIMPLE = re.compile(r'^[AMR]\s+(?P<name>.+)')
GIT_STATUS_SIMPLE_ALL = re.compile(r'^[ AMR]{1,2}\s+(?P<name>.+)')


def extract_git_status_filename(line, only_added=True):
    '''
    Extract target filename from `git status --porcelain` output line.

    For example, extract 'filename' from the following strings:

        'A filename'
        'M filename'
        'R filename.ori -> filename'

        The following below is ignored unless only_added=False:
        ' A filename'
        ' M filename'
        ' R filename.ori -> filename'
        'AM filename'
        'RM filename.ori -> filename'

        This is ignored: (files not part of repo)
        '?? filename'
    '''
    regex = GIT_STATUS_SIMPLE if only_added else GIT_STATUS_SIMPLE_ALL
    match = regex.match(line)
    if match:
        return match.group('name')
    return None


def matches_file(file_name, match_files):
    file_name = file_name.replace('\\', '/')
    return any(re.compile(m).match(file_name) for m in match_files)


def files_ok(files, check):
    print ANSI_BOLD + '*', check['output'] + ANSI_RESET
    status = True
    errcount = 0
    for file_name in files:
        x = check['match_files']
        if x and not matches_file(file_name, x):
            continue

        x = check.get('ignore_files', None)
        if x and matches_file(file_name, x):
            continue

        x = check['command']
        if type(x).__name__ == 'function':
            out, err = x(file_name)
        else:
            process = subprocess.Popen(check['command'] % file_name,
                                       stdout=subprocess.PIPE,
                                       stderr=subprocess.PIPE,
                                       shell=True)
            out, err = process.communicate()

        if out or err:
            if check['print_filename']:
                prefix = '  %s: ' % file_name
            else:
                prefix = '  '
            output_lines = ['%s%s' % (prefix, line) for line in out.splitlines()]
            print '\n'.join(output_lines)
            if err:
                err_lines = ['%s%s' % (prefix, line) for line in err.splitlines()]
                print '\n'.join(err_lines)
            status = False
            errcount += len(output_lines)
            if errcount > MAX_ERROR:
                print '\nStopped. %d errors found and still counting.\n' % errcount
                return status

    return status


def check_files(args):
    try:
        files = []
        if args.all_files:
            for root, dirs, file_names in os.walk('.'):
                for file_name in file_names:
                    files.append(os.path.join(root, file_name))
        else:
            p = subprocess.Popen(['git', 'status', '--porcelain'], stdout=subprocess.PIPE)
            out, err = p.communicate()
            for line in out.splitlines():
                only_added = True
                if args.unstaged:
                    only_added = False
                fname = extract_git_status_filename(line, only_added)
                if fname and os.path.isfile(fname):
                    files.append(fname)

        if not files:
            print '$$ no files to check'
            return

        ok = True
        ok_list = []
        for check in CHECKS:
            if args.full_check:
                ok_list.append(files_ok(files, check))
            else:
                ok = ok and files_ok(files, check)
    finally:
        pass

    if ok_list:
        ok = all(ok_list)
    if not ok:
        print '$$ error'
        raise SystemExit(1)
    print '$$ done'


def install():

    def cmd_output(*args, **kwargs):
        kwargs['stdout'] = subprocess.PIPE
        kwargs['stderr'] = subprocess.PIPE
        process = subprocess.Popen(*args, **kwargs)
        output, unused_err = process.communicate()
        process.poll()
        return output

    try:
        import flake8
        str(flake8.__file__)  # hide 'imported but unused' warning
    except ImportError:
        print "Missing flake8, please install:"
        print "  sudo pip install -U flake8"
        raise SystemExit(1)

    try:
        cmd_output(["npm", "--version"])
    except OSError:
        print "Missing node.js, please install:"
        print "  from http://nodejs.org/ or apt-get install npm"
        raise SystemExit(1)

    try:
        cmd_output(["jshint", "--version"])
    except OSError:
        print "Missing jshint, please install:"
        print "  sudo npm install -g jshint"
        raise SystemExit(1)

    try:
        cmd_output(["puppet", "--version"])
    except OSError:
        print "Missing puppet, please install:"
        print "  sudo apt-get install puppet"
        raise SystemExit(1)

    script = os.path.abspath(__file__)
    repo_root = find_dotgit()
    link = os.path.join(repo_root, ".git", "hooks", "pre-commit")

    try:
        os.remove(link)
    except OSError:
        # Ignore non-existing link
        pass

    print 'link %r -> %r' % (link, script)
    os.symlink(script, link)


def find_dotgit():
    old_folder = os.getcwd()
    while True:
        if os.path.exists(os.path.join(old_folder, '.git')):
            return old_folder
        folder = os.path.split(old_folder)[0]
        if folder == old_folder:
            raise RuntimeError("Can't find .git folder")
        old_folder = folder


def main():
    parser = ArgumentParser(
        description='Check source code before committing.')

    parser.add_argument(
        '-i', '--install',
        action='store_true',
        help='install this script as pre-commit hook; run it under a git repo')

    parser.add_argument(
        '-u', '--unstaged',
        action='store_true',
        help='include files not staged for commit')

    parser.add_argument(
        '-a', '--all-files',
        action='store_true',
        help='all files on disk as is including files not in repo')

    parser.add_argument(
        '-f', '--full-check',
        action='store_true',
        default=False,
        help="run through all checks; don't stop at one check if errors")

    args = parser.parse_args()

    if args.install:
        install()
    else:
        check_files(args)



if __name__ == '__main__':
    main()


# python -m unittest precommitcheck
class Test(unittest.TestCase):

    def test_extract_git_status_filename(self):
        f = extract_git_status_filename
        self.assertEqual(f('A filename'), 'filename')
        self.assertEqual(f('M filename'), 'filename')
        self.assertEqual(f('R filename.ori -> filename'), 'filename')
        self.assertEqual(f(' A filename'), 'filename')
        self.assertEqual(f(' M filename'), 'filename')
        self.assertEqual(f(' R filename.ori -> filename'), 'filename')
        self.assertEqual(f('AM filename'), 'filename')
        self.assertEqual(f('RM filename.ori -> filename'), 'filename')
        self.assertEqual(f('?? filename'), 'filename')

